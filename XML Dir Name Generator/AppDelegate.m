//
//  AppDelegate.m
//  XML Dir Name Generator
//
//  Created by Gene Crucean on 2/25/12.
//  Copyright (c) 2012 Gene Crucean. All rights reserved.
//

#import "AppDelegate.h"

#define logOutputLabelDisplayTime 3

@implementation AppDelegate

@synthesize window = _window;
@synthesize _pathToDirectoryTextField;
@synthesize _pathToSavedXmlFileTextField;
@synthesize _xml;
@synthesize _xmlDoc;
@synthesize _recursiveToggle;
@synthesize _currentDirectory;
@synthesize _logLabel;


- (void)dealloc
{
    [super dealloc];
    [_pathToDirectoryTextField release];
    [_pathToSavedXmlFileTextField release];
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
    
}


- (void)showAlert:(NSString *)message infoText:(NSString *)body didEndSelector:(NSString *)methodName addButton:(NSString *)buttonTitle addButton2:(NSString *)buttonTitle2
{
    NSAlert *alert = [[[NSAlert alloc] init] autorelease];
    [alert addButtonWithTitle:buttonTitle];
    if (buttonTitle2)
    {
        [alert addButtonWithTitle:buttonTitle2];
    }
    [alert setMessageText:message];
    [alert setInformativeText:body];
    [alert setAlertStyle:NSWarningAlertStyle];
    
    
//    [alert beginSheetModalForWindow:self.window modalDelegate:self didEndSelector:methodName contextInfo:nil];
}


#pragma mark - ACTIONS


- (IBAction)goButton:(id)sender
{
//    LOG1 ? NSLog(@"go!") : nil;
//    LOG1 ? NSLog(@"Dir IN:%@", _pathToDirectoryTextField.stringValue) : nil;
//    LOG1 ? NSLog(@"File OUT:%@", _pathToSavedXmlFileTextField.stringValue) : nil;
    
    if ([_pathToDirectoryTextField.stringValue isEqualToString:@""] || [_pathToSavedXmlFileTextField.stringValue isEqualToString:@""])
    {
        LOG1 ? NSLog(@"Both text fields are required.") : nil;
        
        NSAlert *alert = [[[NSAlert alloc] init] autorelease];
        [alert addButtonWithTitle:@"OK"];
        [alert setMessageText:@"Both text fields are required"];
        [alert setInformativeText:@"Make sure both text fields contain valid paths."];
        [alert setAlertStyle:NSWarningAlertStyle];
        [alert beginSheetModalForWindow:self.window modalDelegate:self didEndSelector:@selector(alertDidEnd:returnCode:contextInfo:) contextInfo:nil];
    }
    else
    {
        if (_recursiveToggle.state == 0) // Non-recursive.
        {
            [self nonRecursiveMapper];
        }
        else if (_recursiveToggle.state == 1) // Recursive.
        {
            [self recursiveMapper];
        }
    }
}



- (void)nonRecursiveMapper
{
//    [_fileArray removeAllObjects];
//    [_dirArray removeAllObjects];
    
    _xml = nil;
    _dirArray = nil;
    _fileArray = nil;
    _dirArray = [[NSMutableArray alloc] init];
    _fileArray = [[NSMutableArray alloc] init];
    
    NSError *error;
    NSFileManager *filemgr = [NSFileManager defaultManager];
    NSArray *filelist = [filemgr contentsOfDirectoryAtPath:[_pathToDirectoryTextField stringValue] error:&error];
    
    for (int i = 0; i < filelist.count; i++)
    {
        //            NSLog(@"Found:%@", [filelist objectAtIndex:i]);
        
        BOOL isDir = NO;
        NSString *file = [NSString stringWithFormat:@"%@", [self removeBitsOfString:[filelist objectAtIndex:i]]];
        NSString *fileWithPath = [NSString stringWithFormat:@"%@%@", [_pathToDirectoryTextField stringValue], [self removeBitsOfString:[filelist objectAtIndex:i]]];
        
        BOOL exists = [filemgr fileExistsAtPath:fileWithPath isDirectory:&isDir];
        if (exists)
        {
            if ([file hasPrefix:@"."])
            {
                //                        NSLog(@"File started with a dot and has been skipped.");
            }
            else
            {
                if (!isDir)
                {
                    //                    NSLog(@"is file:%@", file); // DEBUG.
                    [_fileArray addObject:file];
                }
                if (isDir)
                {
                    //                    NSLog(@"is directory:%@", file); // DEBUG.
                    [_dirArray addObject:file];
                }
            }
        }
    }
    
    // Create XML structure.
    _xml = [[NSXMLElement alloc] initWithName:@"xml"];
    
    // This is how to add an attribute.
    //        [xml addAttribute:[NSXMLNode attributeWithName:@"Attribute1" stringValue:@"Value1"]];
    
    if (_dirArray.count > 0)
    {
        NSXMLElement *directories = [[NSXMLElement alloc] initWithName:@"directories"];
        [_xml addChild:directories];
        
        for (int i = 0; i < _dirArray.count; i++)
        {
            NSXMLElement *dir = [[NSXMLElement alloc] initWithName:@"dir"];
            [directories addChild:dir];
            [dir setStringValue:[_dirArray objectAtIndex:i]];
            [dir release];
        }
        [directories release];
    }
    
    if (_fileArray.count > 0)
    {
        NSXMLElement *files = [[NSXMLElement alloc] initWithName:@"files"];
        [_xml addChild:files];
        
        for (int i = 0; i < _fileArray.count; i++)
        {
            NSXMLElement *file = [[NSXMLElement alloc] initWithName:@"file"];
            [files addChild:file];
            [file setStringValue:[_fileArray objectAtIndex:i]];
            [file release];
        }
        [files release];
    }
    
    
    NSLog(@"XML:%@", _xml);
    _xmlDoc = [NSXMLDocument documentWithRootElement:_xml];
    
    // Write XML to file.
    NSData *xmlData = [_xmlDoc XMLDataWithOptions:NSXMLNodePrettyPrint];
    if (![xmlData writeToFile:[_pathToSavedXmlFileTextField stringValue] atomically:YES])
    {
        NSBeep();
        NSLog(@"Could not write document out...");
        [self setLogOutputLabel:@"Something failed during file write!" seconds:logOutputLabelDisplayTime];
        [_logLabel setBackgroundColor:[NSColor colorWithSRGBRed:0.9f green:0.2f blue:0.2f alpha:1.0f]];
    }
    
    //        [_logLabel setStringValue:@"DING!"];
    [self setLogOutputLabel:@"Success!!" seconds:logOutputLabelDisplayTime];
    [_logLabel setBackgroundColor:[NSColor colorWithSRGBRed:0.0f green:1.0f blue:0.5f alpha:1.0f]];
    
    [_xml release];
    [_fileArray release];
    [_dirArray release];
}


- (void)recursiveMapper
{
    if (_dirArrayRecursive != nil)
    {
        _dirArrayRecursive = nil;
    }
    if (_fileArrayRecursive != nil)
    {
        _fileArrayRecursive = nil;
    }

    _dirArrayRecursive = [[NSMutableArray alloc] init];
    _fileArrayRecursive = [[NSMutableArray alloc] init];
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    NSDirectoryEnumerator *filelist = [filemgr enumeratorAtPath:[_pathToDirectoryTextField stringValue]];
    
    [_dirArrayRecursive addObject:[_pathToDirectoryTextField stringValue]]; // Add root directory.
    
    NSString *file;
    while ((file = [filelist nextObject]))
    {
        BOOL isDir = NO;
        NSString *currentFile = [NSString stringWithFormat:@"%@", file];
        NSString *fileWithPath = [NSString stringWithFormat:@"%@%@", [_pathToDirectoryTextField stringValue], file];
        
        //                NSLog(@"currentFile:%@", currentFile);
        //                NSLog(@"fileWithPath:%@", fileWithPath);
        
        //                NSLog(@"goButton: fileWithPath:%@", fileWithPath); // DEBUG.
        
        BOOL exists = [filemgr fileExistsAtPath:fileWithPath isDirectory:&isDir];
        if (exists)
        {
            if ([[currentFile lastPathComponent] hasPrefix:@"."])
            {
                //                        NSLog(@"File started with a dot and has been skipped.");
            }
            else
            {
                // IS FILE.
                if (!isDir)
                {
//                    NSLog(@"is file:%@", currentFile); // DEBUG.
                    [_fileArrayRecursive addObject:currentFile];
                }
                // IS DIRECTORY.
                if (isDir)
                {
                    NSLog(@"is directory:%@", [currentFile lastPathComponent]); // DEBUG.
                    
                    if ([fileWithPath hasSuffix:@"/"])
                    {
                        [_dirArrayRecursive addObject:fileWithPath];
                    }
                    else
                    {
                        [_dirArrayRecursive addObject:[fileWithPath stringByAppendingString:@"/"]];
                    }
                }
            }
        }
    }
    
    NSLog(@"_dirArrayRecursive:%@", _dirArrayRecursive);
    NSLog(@"_fileArrayRecursive:%@", _fileArrayRecursive);
//    NSLog(@"_dirArrayRecursive.count:%lu", _dirArrayRecursive.count);
    
    for (int i = 0; i < _dirArrayRecursive.count; i++)
    {
//        NSLog(@"recursiveContentsChecker:%@%@",[_pathToDirectoryTextField stringValue], [_dirArrayRecursive objectAtIndex:i]);
//        [self recursiveContentsChecker:[NSString stringWithFormat:@"%@%@", [_pathToDirectoryTextField stringValue], [_dirArrayRecursive objectAtIndex:i]] withPath:_fileWithPath];
        [self recursiveContentsChecker:[_dirArrayRecursive objectAtIndex:i]];
    }
    
    [_dirArrayRecursive release];
    [_fileArrayRecursive release];
}

- (IBAction)newWindowMenu:(id)sender
{
    LOG1 ? NSLog(@"newWindowMenu") : nil;
    [_window makeKeyAndOrderFront:self];
}

- (void)recursiveContentsChecker:(NSString *)dirPath
{
//    NSLog(@"::>>::>>::>> filename:%@", filename);
    NSLog(@"::>>::>>::>> dirPath:%@", dirPath);
    
    _xml = nil;
    _dirArray = nil;
    _fileArray = nil;
    _dirArray = [[NSMutableArray alloc] init];
    _fileArray = [[NSMutableArray alloc] init];
    
    NSError *error;
    NSFileManager *filemgr = [NSFileManager defaultManager];
    NSArray *filelist = [filemgr contentsOfDirectoryAtPath:dirPath error:&error];
    
    for (int i = 0; i < filelist.count; i++)
    {
        //            NSLog(@"Found:%@", [filelist objectAtIndex:i]);
        
        BOOL isDir = NO;
        NSString *file = [NSString stringWithFormat:@"%@", [self removeBitsOfString:[filelist objectAtIndex:i]]];
        NSString *fileWithPath = [NSString stringWithFormat:@"%@%@", dirPath, [self removeBitsOfString:[filelist objectAtIndex:i]]];
        
        BOOL exists = [filemgr fileExistsAtPath:fileWithPath isDirectory:&isDir];
        if (exists)
        {
            if ([file hasPrefix:@"."])
            {
                //                        NSLog(@"File started with a dot and has been skipped.");
            }
            else
            {
                if (!isDir)
                {
                    //                    NSLog(@"is file:%@", file); // DEBUG.
                    [_fileArray addObject:file];
                }
                if (isDir)
                {
                    //                    NSLog(@"is directory:%@", file); // DEBUG.
                    [_dirArray addObject:file];
                }
            }
        }
    }
    
    // Create XML structure.
    _xml = [[NSXMLElement alloc] initWithName:@"xml"];
    
    // This is how to add an attribute.
    //        [xml addAttribute:[NSXMLNode attributeWithName:@"Attribute1" stringValue:@"Value1"]];
    
    if (_dirArray.count > 0)
    {
        NSXMLElement *directories = [[NSXMLElement alloc] initWithName:@"directories"];
        [_xml addChild:directories];
        
        for (int i = 0; i < _dirArray.count; i++)
        {
            NSXMLElement *dir = [[NSXMLElement alloc] initWithName:@"dir"];
            [directories addChild:dir];
            [dir setStringValue:[_dirArray objectAtIndex:i]];
            [dir release];
        }
        [directories release];
    }
    
    if (_fileArray.count > 0)
    {
        NSXMLElement *files = [[NSXMLElement alloc] initWithName:@"files"];
        [_xml addChild:files];
        
        for (int i = 0; i < _fileArray.count; i++)
        {
            NSXMLElement *file = [[NSXMLElement alloc] initWithName:@"file"];
            [files addChild:file];
            [file setStringValue:[_fileArray objectAtIndex:i]];
            [file release];
        }
        [files release];
    }
    
    
    NSLog(@"XML:%@", _xml);
    _xmlDoc = [NSXMLDocument documentWithRootElement:_xml];
    
    // Write XML to file.
    NSData *xmlData = [_xmlDoc XMLDataWithOptions:NSXMLNodePrettyPrint];
    
    if (![xmlData writeToFile:[NSString stringWithFormat:@"%@%@", dirPath, [[_pathToSavedXmlFileTextField stringValue] lastPathComponent]] atomically:YES])
    {
        NSBeep();
        NSLog(@"Could not write document out...");
        [self setLogOutputLabel:@"Something failed during file write!" seconds:logOutputLabelDisplayTime];
        [_logLabel setBackgroundColor:[NSColor colorWithSRGBRed:0.9f green:0.2f blue:0.2f alpha:1.0f]];
    }
    
    [self setLogOutputLabel:@"Success!!" seconds:logOutputLabelDisplayTime];
    [_logLabel setBackgroundColor:[NSColor colorWithSRGBRed:0.0f green:1.0f blue:0.5f alpha:1.0f]];
    
    [_xml release];
    [_fileArray release];
    [_dirArray release];
}


- (void)saveToXml:(NSString *)withPath
{
    NSLog(@"saveToXml");
    NSLog(@"_dirArray:%@", _dirArray);
    NSLog(@"_fileArray:%@", _fileArray);
    
    // Create XML structure.
    if (_xml != nil)
    {
        _xml = nil;
    }
    _xml = [[NSXMLElement alloc] initWithName:@"xml"];
    
    // This is how to add an attribute.
    //        [xml addAttribute:[NSXMLNode attributeWithName:@"Attribute1" stringValue:@"Value1"]];
    
    if (_dirArray.count > 0)
    {
        NSXMLElement *directories = [[NSXMLElement alloc] initWithName:@"directories"];
        [_xml addChild:directories];
        
        for (int i = 0; i < _dirArray.count; i++)
        {
            NSXMLElement *dir = [[NSXMLElement alloc] initWithName:@"dir"];
            [directories addChild:dir];
            [dir setStringValue:[_dirArray objectAtIndex:i]];
            [dir release];
        }
        [directories release];
    }
    
    if (_fileArray.count > 0)
    {
        NSXMLElement *files = [[NSXMLElement alloc] initWithName:@"files"];
        [_xml addChild:files];
        
        for (int i = 0; i < _fileArray.count; i++)
        {
            NSXMLElement *file = [[NSXMLElement alloc] initWithName:@"file"];
            [files addChild:file];
            [file setStringValue:[_fileArray objectAtIndex:i]];
            [file release];
        }
        [files release];
    }
    
    
    NSLog(@"XML:%@", _xml);
    _xmlDoc = [NSXMLDocument documentWithRootElement:_xml]; // Maybe try autorelease?
    
    // Write XML to file.
    NSData *xmlData = [_xmlDoc XMLDataWithOptions:NSXMLNodePrettyPrint];
    if (![xmlData writeToFile:withPath atomically:YES])
    {
        NSBeep();
        NSLog(@"Could not write document out...");
        [self setLogOutputLabel:@"Something failed during file write!" seconds:logOutputLabelDisplayTime];
    }
    
    //        [_logLabel setStringValue:@"DING!"];
    [self setLogOutputLabel:@"Whew! File output was a success!!" seconds:logOutputLabelDisplayTime];
    [_logLabel setBackgroundColor:[NSColor colorWithSRGBRed:0.0f green:1.0f blue:0.5f alpha:1.0f]];
    
    [_xml release];
//    [_xmlDoc release]; // Hmm.
    [_fileArray release];
    [_dirArray release];
}


- (IBAction)cleanButton:(id)sender
{
    NSError *error;
    NSFileManager *filemgr = [NSFileManager defaultManager];
    NSDirectoryEnumerator *filelist = [filemgr enumeratorAtPath:[_pathToDirectoryTextField stringValue]];
    
//    NSLog(@"NSDirectoryEnumerator:%@", filelist);
    
    NSString *file;
    while ((file = [filelist nextObject]))
    {
//        NSLog(@"NSDirectoryEnumerator FILE:%@", file);
        if ([[file lastPathComponent] isEqualToString:[_pathToSavedXmlFileTextField stringValue].lastPathComponent])
        {
            NSString *tmpPath = [NSString stringWithFormat:@"%@%@", [_pathToDirectoryTextField stringValue], file];
            NSLog(@"contents.xml FOUND at:%@", tmpPath);
            [filemgr removeItemAtPath:tmpPath error:&error];
        }
    }
    
    [self setLogOutputLabel:@"Clean was successful!!" seconds:logOutputLabelDisplayTime];
    [_logLabel setBackgroundColor:[NSColor colorWithSRGBRed:0.0f green:1.0f blue:0.5f alpha:1.0f]];
}


- (void)setLogOutputLabel:(NSString *)msg seconds:(int)seconds
{
//    NSLog(@"setLogOutputLabel");
    [_logLabel setStringValue:msg];
    [NSTimer scheduledTimerWithTimeInterval:seconds target:self selector:@selector(animateLogLabelOff) userInfo:nil repeats:NO];
}



- (void)animateLogLabelOff
{
//    NSLog(@"animateLogLabelOff");
    [_logLabel setStringValue:@""];
    [_logLabel setBackgroundColor:[NSColor colorWithSRGBRed:1.0f green:1.0f blue:1.0f alpha:1.0f]];
    [_logLabel setTextColor:[NSColor colorWithSRGBRed:0.0f green:0.0f blue:0.0f alpha:1.0f]];
}


- (NSString *)removeBitsOfString:(NSString *)oldString
{
    NSString *str1 = [NSString stringWithFormat:@"%@", oldString];
    NSString *str2 = [NSString stringWithFormat:@"%@", [str1 stringByReplacingOccurrencesOfString:@"file://localhost" withString:@""]];
//    NSLog(@"%@", str2); // DEBUG.
    return str2;
}




#pragma mark - DELEGATES

- (void)alertDidEnd:(NSAlert *)alert returnCode:(NSInteger)returnCode contextInfo:(void *)contextInfo
{
    LOG1 ? NSLog(@"alertDidEnd") : nil;
    
    if (returnCode == NSAlertFirstButtonReturn)
    {
//        [self deleteRecord:currentRec];
        LOG1 ? NSLog(@"NSAlertSecondButtonReturn") : nil;
    }
    else if (returnCode == NSAlertSecondButtonReturn)
    {
        LOG1 ? NSLog(@"NSAlertSecondButtonReturn") : nil;
    }
    else if (returnCode == NSAlertDefaultReturn)
    {
        LOG1 ? NSLog(@"NSAlertDefaultReturn") : nil;
    }
}



#pragma mark -
#pragma mark Browser Dialog's


/////////////////////////////////////////////////////////////////////////////
// File Open Dialog
/////////////////////////////////////////////////////////////////////////////
- (IBAction)fileOpenClick:(id)sender
{
    //    LOG3 ? NSLog(@"~~>>%@ :%@", self.nibName, NSStringFromSelector(_cmd)) : nil;
    LOG1 ? NSLog(@"fileOpenClick") : nil;
    
    NSMutableArray *fileTypes = [[NSMutableArray alloc] initWithCapacity:1];
    
    NSOpenPanel *openDlg = [NSOpenPanel openPanel];
    
    [openDlg setCanChooseFiles:YES];
    [openDlg setCanChooseDirectories:YES];
    [openDlg setCanCreateDirectories:YES];
    
//    [fileTypes addObject:@"ass"];
//    [fileTypes addObject:@""];
//    [openDlg setAllowedFileTypes:fileTypes];
//    NSLog(@"Allowed fileTypes:%@", fileTypes);
    
    if ([openDlg runModal] == NSOKButton)
    {
//        NSArray *files = [openDlg filenames];
        NSArray *files = [openDlg URLs]; // TO REVIEW: Might need to remove the first 6 characters in string.
        
        for (int i = 0; i < [files count]; i++)
        {
            NSString *fileName = [files objectAtIndex:i];
            NSLog(@"//////// fileName ////////// %@", fileName);
            
            if ([sender tag] == 1)
            {
                NSMutableString *path = [NSMutableString stringWithFormat:@"%@", [self removeBitsOfString:fileName]];
                
                NSRange match;
                match = [path rangeOfString:@"%20"];
                
                if (match.location == NSNotFound)
                {
                    LOG1 ? NSLog (@"Match not found") : nil;
                }
                else
                {
                    LOG1 ? NSLog (@"match found at index %lu", match.location) : nil;
//                    [path replaceCharactersInRange:[path rangeOfString:@"%20"] withString:@" "]; // 1
                    [path replaceOccurrencesOfString:@"%20" withString:@" " options:NSLiteralSearch range:NSMakeRange(0, path.length)];
                }
                
                [self._pathToDirectoryTextField setStringValue:path]; // 2
                [self._pathToSavedXmlFileTextField setStringValue:[NSString stringWithFormat:@"%@%@", path, @"contents.xml"]]; // 3
            }
            else if ([sender tag] == 2)
            {
                // There is no 2 in this app. 
            }
        }
    }
    
    
    [fileTypes release];
}


/////////////////////////////////////////////////////////////////////////////
// File Save Dialog
/////////////////////////////////////////////////////////////////////////////
- (IBAction)fileSaveClick:(id)sender
{
    LOG1 ? NSLog(@"fileSaveClick") : nil;
    NSSavePanel* saveDlg = [NSSavePanel savePanel];
    
    [saveDlg setCanCreateDirectories:YES];
    
    //    NSMutableArray *fileTypesBuild = [[NSMutableArray alloc] initWithCapacity:1];
    //    NSMutableArray *fileTypes = [[NSMutableArray alloc] initWithCapacity:1];
    //    
    //    [fileTypesBuild addObject:@"jpg"];
    //    [fileTypesBuild addObject:@"exr"];
    //    [fileTypesBuild addObject:@"tif"];
    //    [fileTypes addObject:fileTypesBuild];
    //    [saveDlg setAllowedFileTypes:fileTypes];
    //    
    //    NSLog(@"Allowed fileTypes:%@", fileTypes);
    
    
    //    if ( [saveDlg runModalForDirectory:nil file:nil] == NSOKButton )
    if ([saveDlg runModal] == NSOKButton)
    {
//        NSString* files = [saveDlg filename];
        NSString *path = [NSString stringWithFormat:@"%@", [saveDlg URL]]; // TO REVIEW: Might need to remove the first 6 characters in string.
        
        NSMutableString *fileNameOut = [NSMutableString stringWithFormat:@"%@", [self removeBitsOfString:path]];
        [fileNameOut replaceCharactersInRange:[fileNameOut rangeOfString:@"%20"] withString:@" "];
        [fileNameOut replaceOccurrencesOfString:@"%20" withString:@" " options:NSLiteralSearch range:NSMakeRange(0, [fileNameOut length])];
        
        if ([sender tag] == 11)
        {
            NSString *str0 = [self removeBitsOfString:fileNameOut];
            NSString *str1 = [str0 stringByDeletingPathExtension];
            NSString *str2 = [str1 stringByAppendingPathExtension:@"xml"];
            [self._pathToSavedXmlFileTextField setStringValue:str2];
        }
    }
}










@end
