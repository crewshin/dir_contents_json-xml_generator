//
//  main.m
//  XML Dir Name Generator
//
//  Created by Gene Crucean on 2/25/12.
//  Copyright (c) 2012 Gene Crucean. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
