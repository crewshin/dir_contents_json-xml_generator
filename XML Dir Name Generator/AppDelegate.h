//
//  AppDelegate.h
//  XML Dir Name Generator
//
//  Created by Gene Crucean on 2/25/12.
//  Copyright (c) 2012 Gene Crucean. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface AppDelegate : NSObject <NSApplicationDelegate, NSAlertDelegate>
{
    NSXMLElement *_xml;
    NSXMLDocument *_xmlDoc;
    NSString *_currentDirectory;
    
    NSMutableString *_file;
    NSMutableString *_fileWithPath;
    
    NSMutableArray *_dirArray;
    NSMutableArray *_fileArray;
    
    NSMutableArray *_dirArrayRecursive;
    NSMutableArray *_fileArrayRecursive;
}

@property (assign) IBOutlet NSWindow *window;
@property (assign) IBOutlet NSTextField *_pathToDirectoryTextField;
@property (assign) IBOutlet NSTextField *_pathToSavedXmlFileTextField;
@property (assign) NSXMLElement *_xml;
@property (assign) NSXMLDocument *_xmlDoc;
@property (assign) IBOutlet NSButton *_recursiveToggle;
@property (assign) NSString *_currentDirectory;
@property (assign) IBOutlet NSTextField *_logLabel;


- (IBAction)fileOpenClick:(id)sender;
- (IBAction)fileSaveClick:(id)sender;
- (IBAction)goButton:(id)sender;
- (void)alertDidEnd:(NSAlert *)alert returnCode:(NSInteger)returnCode contextInfo:(void *)contextInfo;
//- (NSArray *)allFiles:(NSString *)startPath;
- (NSString *)removeBitsOfString:(NSString *)oldString;
- (void)showAlert:(NSString *)message infoText:(NSString *)body didEndSelector:(NSString *)methodName addButton:(NSString *)buttonTitle addButton2:(NSString *)buttonTitle2;
- (void)setLogOutputLabel:(NSString *)msg seconds:(int)seconds;
- (IBAction)cleanButton:(id)sender;
- (void)recursiveContentsChecker:(NSString *)dirPath;
- (void)saveToXml:(NSString *)withPath;
- (void)nonRecursiveMapper;
- (void)recursiveMapper;

- (IBAction)newWindowMenu:(id)sender;


@end
